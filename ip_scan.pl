#!/usr/bin/perl
#Written by Forrest Hooker, 02/25/2023
#Very simple perl script to scan for all available hosts on current network.
#First the user's current IP address is snagged for the first 3 octets (x.x.x.y),
#then an array is created of all possible addresses (x.x.x.1-245), and each is pinged.


#Syntax Pragma
use strict;
use diagnostics;
use warnings;
use 5.010;

#Used to grab IP from localhost
use Net::Address::IP::Local;
#Used to ping each possible host
use Net::Ping;
#Used for colored output on shell.
use Term::ANSIColor qw(:constants);
#Temporary module I'm using to do bash-like array checks without using grep. Will probably be removed.
use experimental 'smartmatch';

## Vars ##

#Define $packet_type as the first given user argument. Allows user to set a protocol for the ping check packets.
#NEED TO RENAME AS $packet_type
my $packet_type = $ARGV[0];

#Define $packet_len as the second given user argument. Allows user to specify a timeout if certain hosts are showing as unreachable.
my $packet_len = $ARGV[1];

#Grab local IPv4 Address
my $address = Net::Address::IP::Local->public_ipv4;
#Grab only first 3 octets of the local IP address
$address =~s/\.[^.]*$//;

#define $pong as a ping command from the Net pragma
#my $pong = Net::Ping->new("$packet",$packet_len);

#Declare @hosts_arr as an array of 1-255 to use for the last octet in scanning
my @hosts_arr = (1..255);

#Declare @dhcp_range as an empty array that ip_scan() will add to in order to create a possible dhcp range.
my @dhcp_range = ();

#Array of protocols for Net:: Ping command (Default is tcp). ICMP CANNOT BE USED WITHOUT SUDO.
my @protocols=("tcp", "icmp", "udp", "syn");

#Array of possible help flags user might give in order to display usage
my @help_flags=("help", "HELP", "Help", "usage", "USAGE", "Usage", "-h", "--h", "-H", "--H"); 

## Code ##

#Main function.
sub __main__ {
	#Argument Check sub-routine.
	arg_check();
	#IP scanning sub-routine.
	ip_scan();
} #End __main__


#Argument Check sub-routine. Checks all user given arguments and sets defaults if there are none.
sub arg_check {
	
	##   $packet_type check   ##
	
	#If no argument has been given for $packet, set defaults.
	if ( ! defined $packet_type){
		#Notify user that a packet type has not been specified and the default protocol of TCP will be used.
		print YELLOW, BOLD,"\nPacket protocol not specified, using default TCP packet for scanning.\n", RESET;
		#define $packet with default protocol of TCP
		$packet_type = "tcp";
	#Else, if $packet_type matches any element in @help_flags...
	}elsif ( $packet_type ~~ @help_flags ){
		#Run the _usage_(); sub-routine.
		_usage_();
	#Else, if $packet HAS been defined but does not match the @protocols array...
	}elsif ( ! $packet_type ~~ @protocols){
		#Notify user that $packet is not a valid protocol and give valid working protocols
		print("Unknown packet type of $packet_type - please specify a valid protocol [tcp,icmp,udp,syn]");
		#Notify user that the script is being exited.
		print("Exiting...\n\n");
		#Exit with an error code of 1.
		exit();
	}elsif ( $packet_type eq "icmp" && $< != 0 ){
		#Notify user that ICMP packets can't be used by perl without being root/sudo'd.
		print BOLD, RED, "\nICMP PACKET DETECTED WITHOUT ROOT. Perl does not allow for ICMP packets to be sent without elevated privileges.\n";
		#Notify user ways of using ICMP specifically or just running a simple scan.
		print RED, "\nTo scan using ICMP packets, run sudo ./ip_scan.pl icmp - To scan without sudo, run the command without options for a default protocol of TCP.\n", RESET;
		#Notify user that program is exiting.
		print RED, "\nExitting...\n\n", RESET;
		#Exit.
		exit();
	} #End $packet_type check
	
	##   $packet_len check ##

	#If no value for $packet_len has been specified, then...
	if ( ! defined $packet_len){
		#Notify user
		print YELLOW, BOLD, "\nTimeout length has not been set. Using default of 1 packet.\n", RESET;
		#Set $packet_len to default value of 1 packet 
		$packet_len = 1; 
	#else, if $packet_len HAS been given, convert to integer (Possible unnecessary, this language is so weird man).
	}else{
		$packet_len = int($packet_len);
	} #End $packet_len check
	
	
} #End arg_check sub-routine.
		
	
#The actual scanning portion of the program.
sub ip_scan{	
	#define $pong as a ping command from the Net pragma
	my $pong = Net::Ping->new("$packet_type",$packet_len);
	#Notify user that scan is beginning.
	print WHITE, BOLD, "\nScanning all hosts from $address.1-255 using $packet_len $packet_type packet(s)...\n\n", RESET;
	#For each possible octet in @hosts_arr, do...
	for my $octet (@hosts_arr){
		#For each possible octet in @hosts_arr, define $host as the current octet. (e.g. if octet = 1, define $host as 192.168.1.1)
		my $host = "${address}.${octet}";
		#Weird buffer thing for quicker response time. Need to figure out what this actually does. 
		$|++;
		#Give status update for current host being scanned.
		print("Scanning $host\r");
		#If ping is successful for the defined host, then...
		if ($pong->ping($host)){
			print GREEN, BOLD,"$host is up.       \n", RESET;
			push (@dhcp_range, $host); 
		} #End if statement for ping result
	} #End for loop of each item in @hosts_arr
	print "                        \r";
	print BOLD, WHITE, "\nPossible DHCP Range: $dhcp_range[0]-$dhcp_range[-1]\n\n", RESET;
} #end ip_scan sub-routine


#Usage Module
sub _usage_{
	print WHITE, BOLD, "\nip_scan.pl is a simple utility that scans all neighbors on a network for reachability allowing for the use of several different packet protocols.\n\n", RESET;
	print("./ip_scan.pl [no options] - Default simple scan using singular TCP packet.\n");
	print("./ip_scan.pl [tcp/udp/syn] [0-100] - Scan using defined packet protocol and defined number of packets.\n");
	print "sudo ./ip_scan.pl [icmp] [0-100] - Scan using ICMP packets with custom number of packets ", RED,BOLD,UNDERLINE "(MUST BE RUN AS ROOT!)", RESET, "\n\n";
	print YELLOW, BOLD, "NOTE: The calculated possible DHCP range is literally a guess, it simply tries to establish the range of hosts that were reachable in the scan.\n\n", RESET;
	exit();
} #end _usage_ sub-routine


__main__();

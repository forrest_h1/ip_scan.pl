# ip_scan.pl

ip_scan.pl is my attempt at learning a language I've never touched or knowingly used (Perl) and creating something useful with it in 2 days. The program itself is effectively a modified, exteremely simplified version of nmap, without the port scanning or service recognition, but WITHOUT you needing to define an address range. It allows for the use of 4 protocols to be used with the packet(s); TCP (Default), TCP-SYN, UDP, and ICMP (WIP!!!). It also allows for a defined number of packets to be sent to each host.


The tool also tries to guess what the possible DHCP range based on the first and last reachable host's address: Please note, this feature is entirely guessing and should not be used as a concrete method of determining the DHCP range given by a server.


Running the tool without options (`./ipscan.pl `) will use the default preferences of a single TCP packet to each host to determine reachability. 


## Modules Required (Installed successfully with CPAN)

-Net::Address</br >


-Net::Ping</br >


-experimental 'smartwatch' (Does not need to be installed, but will possibly be switched with grep instead.)
</br > 



## How It Works

Upon running ip_scan, the script will try to grab your current IP address using the Net::Address module. It will remove the last octet, then create a list of IPs using a defined range of 1-255 (I am working to implement the ability to use custom ranges). The script will then attempt to reach each host in the range, either by using the default single TCP packet, or using your defined protocol and number of packets. If a host within the range is reachable, the script will print a green notification with the address, and the address is added to a list. When the range is exhausted, the script will then print out a complete guesstitmate of what the DHCP range could be. 

## Usage

To run a simple scan using the default options, run the command without any options, like so: </br >

`./ip_scan.pl ` </br >


To specify the packet protocol to be used in the scan, give the name of the protocol as the first argument: </br >


`./ip_scan.pl [tcp/udp/icmp***/syn]`</br >


To specify the number of packets sent before a timeout is determined, give the number as an integer after the first argument:</br >


`./ip_scan.pl [tcp/udp/icmp/syn] [0-100]`</br >


Finally, to specify that you'd like to send an ICMP packet, you'll need to run the script as root:


`sudo ./ip_scan.pl [icmp] `</br >


***SINCE THIS WAS NOT OBVIOUS TO ME I'M SLAPPING IT IN HERE. If you've never touched perl or never learned how to use CPAN correctly, you'll need to make sure you install the Net::Address/Ping packages as root. Otherwise, this script will break trying to find the `Net:;` packages you more than likely have installed. Thankfully, this is as simple as running `sudo cpan Net::Address::IP::Local` rather than just `cpan [NAME::OF::PACKAGE]`. </br >



## Features To Add

-IPv6 Support</br >


-All other protocols offered by the Net::Ping module</br >


-Interface selection (currently just grabs the first available local IP)</br >


-Better argument parsing/implementation</br >


-Custom address range</br >


-Possibly add optional port scanning?</br >

